#include <wiringPi.h>
#include <iostream>
#include <cstdlib>

int main(int argc, char * argv[]){
	
	if(argc != 2){
		std::cout << "bad args"<<std::endl;
		return 0;
	}
	int pin = atoi(argv[1]);
	wiringPiSetup();

	pinMode(pin, INPUT);

	while(true){
		int i = digitalRead(pin);
		delay(250);
		std::cout<<i<<std::endl;
	}


	return 0;
}
