#ifndef FISH_TANK_APP_FISHWINDOW_H
#define FISH_TANK_APP_FISHWINDOW_H

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/grid.h>
#include <gtkmm/window.h>
#include <gtkmm/label.h>
#include <glibmm/main.h>

#define MAX_LIGHT_LEVEL 8
#define MIN_LIGHT_LEVEL 0

#include "fishtank.h"
// TODO: include gpio methods and wiring pi

class FishWindow : public Gtk::Window {

public: 
  FishWindow();
  virtual ~FishWindow();

protected:
//Signal Handlers
  bool updateTempurature();
  bool updateWaterLevel();
  void on_button_feed();
  void on_button_up();
  void on_button_down();
  void on_light_toggle();
  bool updateLightLevel();


//Child Elements
  //Layout Elements
  Gtk::Grid grid;
  //Buttons
  Gtk::Button feed_btn;
  Gtk::ToggleButton light_btn;
  Gtk::Button lightup_btn;
  Gtk::Button lightdown_btn;
  // Possible Shutdown button
  Gtk::Label title, lightlevel_lbl, temp_lbl, waterlevel_lbl, light_lbl;

  FishTank fishtank;
};

#endif //FISH_TANK_APP_FISHWINDOW_H
