CXX=g++
CXXFLAGS= -Wall -pedantic -g $(shell pkg-config --cflags gtkmm-3.0) -std=c++14 -lwiringPi
PKG=$(shell pkg-config --cflags --libs gtkmm-3.0)
SOURCE= main.cpp fishwindow.cpp fishtank.cpp
OBJ= main.o fishwindow.o fishtank.o
HEADERS = main.h fishwindow.h fishtank.h
EXE=auto_fish_tank

all: objects

objects: $(OBJ)
	$(CXX) $(CXXFLAGS) $(OBJ) -o $(EXE) $(PKG)

*.o : *.c *.h
	$(CXX) $(CXXFLAGS) $(PKG) -c $(SOURCE) -o $(OBJ) $(PKG)

.PHONY:clean

clean:
	rm -f $(OBJ) $(EXE)

